package com.wallet.entity;


import com.wallet.utils.enums.CurrencyTypeEnum;

import javax.persistence.*;

@Entity
@Table(name = "currency")
public class Currency  {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private CurrencyTypeEnum name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CurrencyTypeEnum getName() {
        return name;
    }

    public void setName(CurrencyTypeEnum name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}
