package com.wallet.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name="document")
public  class Document  {

	private static final long serialVersionUID = 2553407174517962189L;

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private Date docDate;

	private String docDescription;

	private String docRef;

	private Double docAmount;

	@ManyToOne
	private DocumentType documentType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDocDate() {
		return docDate;
	}

	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}

	public String getDocDescription() {
		return docDescription;
	}

	public void setDocDescription(String docDescription) {
		this.docDescription = docDescription;
	}

	public String getDocRef() {
		return docRef;
	}

	public void setDocRef(String docRef) {
		this.docRef = docRef;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public Double getDocAmount() {
		return docAmount;
	}

	public void setDocAmount(Double docAmount) {
		this.docAmount = docAmount;
	}

	@Override
	public String toString() {
		return "Document{" +
				"id=" + id +
				", docDate=" + docDate +
				", docDescription='" + docDescription + '\'' +
				", docRef='" + docRef + '\'' +
				", docAmount=" + docAmount +
				", documentType=" + documentType +
				'}';
	}
}