package com.wallet.entity;


import com.wallet.utils.enums.DocumentTypeEnum;

import javax.persistence.*;

@Entity
@Table(name = "DocumentType")
public class DocumentType{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private DocumentTypeEnum name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DocumentTypeEnum getName() {
        return name;
    }

    public void setName(DocumentTypeEnum name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DocumentType{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}
