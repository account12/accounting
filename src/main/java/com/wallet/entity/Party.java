
package com.wallet.entity;

import com.wallet.utils.EducationGradeEnum;
import com.wallet.utils.PartyGenderEnum;
import com.wallet.utils.PartyStateEnum;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Party")
public class Party  {

	private static final long serialVersionUID = -2662532490984665938L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String firstName;

	private String family;

	@Enumerated(EnumType.ORDINAL)
	private PartyGenderEnum gender;

	private String username;

 	private String password;

	private String email;

	private String mobile;

	@Temporal(TemporalType.DATE)
	private Date birthDate;

	@Enumerated(EnumType.ORDINAL)
	private PartyStateEnum partyState;

	private String postCode;

	private String activationCode;

	@Enumerated(EnumType.ORDINAL)
	private EducationGradeEnum educationGrade;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public PartyGenderEnum getGender() {
		return gender;
	}

	public void setGender(PartyGenderEnum gender) {
		this.gender = gender;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public PartyStateEnum getPartyState() {
		return partyState;
	}

	public void setPartyState(PartyStateEnum partyState) {
		this.partyState = partyState;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public EducationGradeEnum getEducationGrade() {
		return educationGrade;
	}

	public void setEducationGrade(EducationGradeEnum educationGrade) {
		this.educationGrade = educationGrade;
	}

	@Override
	public String toString() {
		return "Party{" +
				"id=" + id +
				", firstName='" + firstName + '\'' +
				", family='" + family + '\'' +
				", gender=" + gender +
				", username='" + username + '\'' +
				", password='" + password + '\'' +
				", email='" + email + '\'' +
				", mobile='" + mobile + '\'' +
				", birthDate=" + birthDate +
				", partyState=" + partyState +
				", postCode='" + postCode + '\'' +
				", activationCode='" + activationCode + '\'' +
				", educationGrade=" + educationGrade +
				'}';
	}
}