package com.wallet.entity;

import com.wallet.utils.enums.TransactionTypeEnum;

import javax.persistence.*;

@Entity
@Table(name = "TransactionType")
public class TransactionType  {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private TransactionTypeEnum name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TransactionTypeEnum getName() {
        return name;
    }

    public void setName(TransactionTypeEnum name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TransactionType{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}
