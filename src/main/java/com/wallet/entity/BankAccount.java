package com.wallet.entity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "bank_account")
public class BankAccount  {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "account_code", nullable = false)
    private String accountCode;

    @NotNull
    @Column(name = "account_number", nullable = false)
    private String accountNumber;

    @Column(name = "balance")
    private Double balance;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "is_creditor", nullable = false)
    private Boolean isCreditor;

    @NotNull
    @Column(name = "is_debtor", nullable = false)
    private Boolean isDebtor;


    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "shaba_number", nullable = false)
    private String shabaNumber;

    @Column(name = "bank", nullable = false)
    private Integer bank;

    @Column(name = "is_cash", nullable = false)
    private Boolean isCash;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @ManyToOne
    private Currency currency;

    @ManyToOne
    private Party party;

    @OneToMany(mappedBy = "account")
    private Set<Transaction> transactions = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getShabaNumber() {
        return shabaNumber;
    }

    public void setShabaNumber(String shabaNumber) {
        this.shabaNumber = shabaNumber;
    }

    public Integer getBank() {
        return bank;
    }

    public void setBank(Integer bank) {
        this.bank = bank;
    }


    public Boolean getCash() {
        return isCash;
    }

    public void setCash(Boolean cash) {
        isCash = cash;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCreditor() {
        return isCreditor;
    }

    public void setCreditor(Boolean creditor) {
        isCreditor = creditor;
    }

    public Boolean getDebtor() {
        return isDebtor;
    }

    public void setDebtor(Boolean debtor) {
        isDebtor = debtor;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "id=" + id +
                ", accountCode='" + accountCode + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", balance=" + balance +
                ", description='" + description + '\'' +
                ", isCreditor=" + isCreditor +
                ", isDebtor=" + isDebtor +
                ", title='" + title + '\'' +
                ", shabaNumber='" + shabaNumber + '\'' +
                ", bank=" + bank +
                ", isCash=" + isCash +
                ", isActive=" + isActive +
                ", currency=" + currency +
                ", party=" + party +
                ", transactions=" + transactions +
                '}';
    }
}
