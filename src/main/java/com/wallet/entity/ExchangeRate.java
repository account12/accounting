package com.wallet.entity;

import javax.persistence.*;

@Entity
@Table(name = "ExchangeRate")
public class ExchangeRate {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double rate;

    @ManyToOne
    private Currency firstCurrency;

    @ManyToOne
    private Currency secondCurrency;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Currency getFirstCurrency() {
        return firstCurrency;
    }

    public void setFirstCurrency(Currency firstCurrency) {
        this.firstCurrency = firstCurrency;
    }

    public Currency getSecondCurrency() {
        return secondCurrency;
    }

    public void setSecondCurrencyId(Currency secondCurrency) {
        this.secondCurrency = secondCurrency;
    }

    @Override
    public String toString() {
        return "ExchangeRate{" +
                "id=" + id +
                ", rate=" + rate +
                ", firstCurrency=" + firstCurrency +
                ", secondCurrency=" + secondCurrency +
                '}';
    }
}
