package com.wallet.controller;

import com.wallet.model.RestResponse;
import com.wallet.model.TransactionDto;
import com.wallet.model.TransferDto;
import com.wallet.service.TransactionService;
import com.wallet.service.WalletService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by taraneh on 7/31/2020.
 */

@RestController
@RequestMapping("/api")
public class TransactionResource {

    private static final Logger logger = LoggerFactory.getLogger(TransactionResource.class);

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private WalletService walletService;


    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/balance/{accountid}/{currencyid}", headers = "Accept=application/json;charset=UTF-8")
    public RestResponse<Long> getBalance(@PathVariable("accountid") Long accountid, @PathVariable("currencyid") Long currencyid) {
        logger.info("do getBalance sid = {} and msisdn= {} , {} ", accountid, currencyid);
        Long balance = transactionService.getBalance(accountid, currencyid);
        return new RestResponse<>(balance, HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/debit", headers = "Accept=application/json;charset=UTF-8")
    public RestResponse<Long> debit(TransactionDto dto) {
        logger.info("do debit : {} ", dto);
        return new RestResponse<>(walletService.debit(dto), HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/credit", headers = "Accept=application/json;charset=UTF-8")
    public RestResponse<Long> credit(TransactionDto dto) {
        logger.info("do credit : {} ", dto);
        return new RestResponse<>(walletService.credit(dto), HttpStatus.OK);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/transfer", headers = "Accept=application/json;charset=UTF-8")
    public RestResponse<Long> transfer(TransferDto dto) {
        logger.info("do transfer : {}", dto);
        return new RestResponse<>(walletService.transfer(dto), HttpStatus.OK);
    }
}
