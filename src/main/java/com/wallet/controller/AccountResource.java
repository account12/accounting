package com.wallet.controller;

import com.wallet.model.BankAccountRequest;
import com.wallet.model.RestResponse;
import com.wallet.service.BankAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by taraneh on 7/31/2020.
 */


@RestController
@RequestMapping("/api")
public class AccountResource {

    private static final Logger logger = LoggerFactory.getLogger(AccountResource.class);

    @Autowired
    private BankAccountService bankAccountService;

    @PostMapping(value = "/create/account")
    public RestResponse<Boolean> createCurrency(@Valid @RequestBody BankAccountRequest bankAccountRequest){
        logger.debug("incoming create registration account request [{}]", bankAccountRequest);
        bankAccountService.createBankAccount(bankAccountRequest);
        return new RestResponse<>(true, HttpStatus.CREATED);
    }

    @PutMapping("/update/account")
    public ResponseEntity<Boolean> updateCurrency(@Valid @RequestBody BankAccountRequest bankAccountRequest) {
        logger.debug("REST request to update account : {}", bankAccountRequest.getAccountCode());
        bankAccountService.updateBankAccount(bankAccountRequest);
        return new RestResponse<>(true, HttpStatus.OK);
    }
}
