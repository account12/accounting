package com.wallet.controller;

import com.wallet.model.CurrencyRequest;
import com.wallet.model.RestResponse;
import com.wallet.service.CurrencyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by taraneh on 7/31/2020.
 */


@RestController
@RequestMapping("/api")
public class CurrencyResource {

    private static final Logger logger = LoggerFactory.getLogger(CurrencyResource.class);

    @Autowired
    private CurrencyService currencyService;


    @PostMapping(value = "/create/currency")
    public RestResponse<Boolean> createCurrency(@Valid @RequestBody CurrencyRequest currencyRequest){
        logger.debug("incoming create registration currency request [{}]", currencyRequest);
        currencyService.createCurrency(currencyRequest);
        return new RestResponse<>(true, HttpStatus.CREATED);
    }

    @PutMapping("/update/currency")
    public ResponseEntity<Boolean> updateCurrency(@Valid @RequestBody CurrencyRequest currencyRequest) {
        logger.debug("REST request to update currency : {}", currencyRequest.getName());
        currencyService.updateCurrency(currencyRequest);
        return new RestResponse<>(true, HttpStatus.OK);
    }


    @DeleteMapping("/delete/currency/{id}")
    public ResponseEntity<Void> deleteCurrency(@PathVariable Long id) {
        logger.debug("REST request to delete Currency : {}", id);
        currencyService.deleteCurrency(id);
        return ResponseEntity.ok().build();
    }
}
