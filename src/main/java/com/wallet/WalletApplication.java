package com.wallet;

import com.wallet.entity.Currency;
import com.wallet.entity.DocumentType;
import com.wallet.entity.TransactionType;
import com.wallet.service.repository.CurrencyRepository;
import com.wallet.service.repository.DocumentTypeRepository;
import com.wallet.service.repository.TransactionTypeRepository;
import com.wallet.utils.enums.CurrencyTypeEnum;
import com.wallet.utils.enums.DocumentTypeEnum;
import com.wallet.utils.enums.TransactionTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;

@SpringBootApplication
@Configuration
//@PropertySource("file:.//application.properties")
public class WalletApplication {

	public static void main(String[] args) {
		SpringApplication.run(WalletApplication.class, args);
	}

	@Component
	class TransactionTypeServiceImpl implements CommandLineRunner {

		@Autowired
		private TransactionTypeRepository transactionTypeRepository;
		@Autowired
		private DocumentTypeRepository documentTypeRepository;
		@Autowired
		private CurrencyRepository currencyRepository;
		@Override
		public void run(String... strings) throws Exception {
			TransactionType credit = new TransactionType();
			credit.setName(TransactionTypeEnum.Credit);
			transactionTypeRepository.save(credit);

			TransactionType debit = new TransactionType();
			debit.setName(TransactionTypeEnum.Debit);
			transactionTypeRepository.save(debit);

			DocumentType creditDoc = new DocumentType();
			creditDoc.setName(DocumentTypeEnum.CREDIT);
			documentTypeRepository.save(creditDoc);

			DocumentType debitDoc = new DocumentType();
			debitDoc.setName(DocumentTypeEnum.DEBIT);
			documentTypeRepository.save(debitDoc);

			Currency dollar = new Currency();
			dollar.setName(CurrencyTypeEnum.DOLLAR);
			currencyRepository.save(dollar);

			Currency rial = new Currency();
			rial.setName(CurrencyTypeEnum.RIAL);
			currencyRepository.save(rial);

		}
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

}
