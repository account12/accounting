package com.wallet.utils.enums;

public enum TransactionTypeEnum {
	Credit(1L),
	Debit(2L);

	private String value;
	private Long id;

	TransactionTypeEnum(Long id){
		this.id = id;
	}

	public Long getId(){
		return id;
	}
	
	public static TransactionTypeEnum fromValue(Long value){
    	if (value == 1){
    		return TransactionTypeEnum.Credit;
    	}else if (value == 2){
    		return TransactionTypeEnum.Debit;
    	}else{
    		return null;
    	}    	
    }
}
