package com.wallet.utils.enums;

public enum CurrencyTypeEnum {

    DOLLAR(1L,"دلار"),
    RIAL(2L, "ریال"),
    EURO(3L, "یورو"),
    POUND(4L, "پوند")
    ;

    private String value;

    private Long  id;

    CurrencyTypeEnum(Long id, String value) {
        this.value = value;
        this.id = id;
    }
    public String getValue() {
        return value;
    }

    public Long getId() {
        return id;
    }

    public static CurrencyTypeEnum fromValue(Long value){
        if (value == 1){
            return CurrencyTypeEnum.DOLLAR;
        }else if (value == 2){
            return CurrencyTypeEnum.POUND;
        }else if (value == 3){
            return CurrencyTypeEnum.EURO;
        }else if (value == 4){
            return CurrencyTypeEnum.RIAL;
        }
        else{
            return null;
        }
    }

}
