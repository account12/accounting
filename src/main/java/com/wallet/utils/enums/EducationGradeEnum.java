package com.wallet.utils;

public enum EducationGradeEnum{

	UnderDiploma(0, "زیر دیپلم"),
	Diploma(1, "دیپلم"),
	Bachelor(2, "کارشناسی"),
	Master(3, "ارشد "),
	Doctorate(4, "دکترا"),
	PostDoctorate(5, "فوق دکترا"),
	MD(6, "پزشکی");

    private String name;

    private Integer id;

	EducationGradeEnum(Integer id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }
    public static EducationGradeEnum fromValue(int value){
    	if (value == 0){
    		return EducationGradeEnum.UnderDiploma;
    	}if (value == 1){
    		return EducationGradeEnum.Diploma;
    	}
    	if (value == 2){
    		return EducationGradeEnum.Bachelor;
    	}
    	if (value == 3){
    		return EducationGradeEnum.Master;
    	}
    	if (value == 4){
    		return EducationGradeEnum.Doctorate;
    	}
    	if (value == 5){
    		return EducationGradeEnum.PostDoctorate;
    	}
    	if (value == 5){
    		return EducationGradeEnum.MD;
    	}else{
    		return EducationGradeEnum.Diploma;
    	}
    }
}
