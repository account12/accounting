package com.wallet.utils.enums;

public enum DocumentTypeEnum {
    NONE(0L), CREDIT(1L), DEBIT(2L);

    private Long id;

    DocumentTypeEnum(Long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }

    public static DocumentTypeEnum fromValue(Long value){
        if (value == 0){
            return DocumentTypeEnum.NONE;
        }else if (value == 1){
            return DocumentTypeEnum.CREDIT;
        }else if (value == 2){
            return DocumentTypeEnum.DEBIT;
        }else{
            return null;
        }


    }
}
