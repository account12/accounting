package com.wallet.utils;

public enum PartyGenderEnum {

	FEMALE(0, "Female"),
	MALE(1, "Male"),
	DontCare(1, "DontCare");

    private String value;

    private Integer  id;

    PartyGenderEnum(Integer id, String value) {
        this.value = value;
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public Integer getId() {
        return id;
    }
    
    public static PartyGenderEnum fromValue(int value){
    	if (value == 0){
    		return PartyGenderEnum.FEMALE;
    	}else if (value == 1){
    		return PartyGenderEnum.MALE;
    	}
    	else if (value == 1){
    		return PartyGenderEnum.DontCare;
    	}else{
    		return null;
    	}
    }
}
