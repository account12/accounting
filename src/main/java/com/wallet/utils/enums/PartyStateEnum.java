package com.wallet.utils;

public enum PartyStateEnum {

	DEACTIVE(0,"غیر فعال"),
	ACTIVE(1, "فعال")	;

    private String value;

    private Integer  id;

    PartyStateEnum(Integer id, String value) {
        this.value = value;
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public Integer getId() {
        return id;
    }
	
	public static PartyStateEnum fromValue(int value){
    	if (value == 1){
    		return PartyStateEnum.ACTIVE;
    	}else if (value == 0){
    		return PartyStateEnum.ACTIVE;
    	}else{
    		return null;
    	}
    	 
    	
    }
}
