package com.wallet.utils;

import com.wallet.entity.Document;
import com.wallet.model.CurrencyRequest;
import com.wallet.model.CurrencyResponse;
import org.hibernate.service.spi.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class ConvertUtil {

    public CurrencyResponse setCurrencyIsOk(){
        CurrencyResponse response = new CurrencyResponse();
        response.setResponseType(HttpStatus.CREATED);
        response.setResponseDesc("عملیات با موفقیت انجام شد");
        return response;
    }





    /*public VoucherDto setVoucher(VoucherDto voucherDto, Voucher voucher) {
        if (voucher != null) {
            voucherDto.setVoucherItems(setVoucherItems(new ArrayList<VoucherItemDto>(), voucher.getVoucherItems()));
            voucherDto.setVoucherAttachments(setVoucherAttachments(new ArrayList<VoucherAttachmentDto>(), voucher.getVoucherAttachments()));
            voucherDto.setId(voucher.getId());
            voucherDto.setCode(voucher.getCode());
            voucherDto.setTitle(voucher.getTitle());
            if (voucher.getDescription() != null)
                voucherDto.setDescription(voucher.getDescription());
            voucherDto.setDate(ir.boursam.accounting.service.util.DateUtil.convertFromGregorianToJalali(voucher.getVoucherDate().toLocalDate()));
            voucherDto.setVoucherType(setVoucherType(new VoucherTypeDto(), voucher.getVoucherType()));
            voucherDto.setVoucherStatus(VoucherStatusDto.valueOf(voucher.getVoucherStatus().name()));
            voucherDto.setAmount(voucher.getVoucherAmount());
            voucherDto.setSubmittedUser(voucher.getCreatedBy());
            voucherDto.setSubmittedDate(ir.boursam.accounting.service.util.DateUtil.convertFromGregorianToJalali(voucher.getCreatedDate().toLocalDate()));
            if (voucher.getVoucherStatus().equals(VoucherStatusDto.APPROVED)) {
                voucherDto.setConfirmedUser(voucher.getLastModifiedBy());
                voucherDto.setConfirmedDate(ir.boursam.accounting.service.util.DateUtil.convertFromGregorianToJalali(voucher.getLastModifiedDate().toLocalDate()));
            }
        }
        return voucherDto;
    }

    public List<VoucherItemDto> setVoucherItems(List<VoucherItemDto> voucherItemDtos, Set<VoucherItem> voucherItems) {
        if (!voucherItems.isEmpty() && voucherItems.size() != 0 *//*&& !voucherItems != null*//*) {
            for (VoucherItem voucherItem : voucherItems)
                voucherItemDtos.add(setVoucherItem(new VoucherItemDto(), voucherItem));
        }
        return voucherItemDtos;
    }

    public Set<VoucherItem> setVoucherItems(Set<VoucherItem> voucherItems, List<VoucherItemDto> voucherItemDtos) {
        if (!voucherItemDtos.isEmpty() && voucherItemDtos.size() != 0 *//*&& !voucherItemDtos != null*//*) {
            for (VoucherItemDto voucherItemDto : voucherItemDtos)
                voucherItems.add(setVoucherItem(new VoucherItem(), voucherItemDto));
        }
        return voucherItems;
    }

    public VoucherItemDto setVoucherItem(VoucherItemDto voucherItemDto, VoucherItem voucherItem) {
        if (voucherItem != null) {
            voucherItemDto.setId(voucherItem.getId());
            voucherItemDto.setTitle(voucherItem.getTitle());
            voucherItemDto.setDebit(voucherItem.getDebit());
            voucherItemDto.setCredit(voucherItem.getCredit());
            voucherItemDto.setAccountId(voucherItem.getAccount().getId());
            voucherItemDto.setAccountNumber(voucherItem.getAccount().getAccountNumber());
            voucherItemDto.setAccountName(voucherItem.getAccount().getName());
            voucherItemDto.setHasTaxInfo(voucherItem.getTaxInfo() != null);
            voucherItemDto.setTaxInfo(setTaxInfo(new TaxInfoDto(), voucherItem.getTaxInfo()));
            voucherItemDto.setAction(ActionDto.UNCHANGED);
        }
        return voucherItemDto;
    }


    public VoucherItem setVoucherItem(VoucherItem voucherItem, VoucherItemDto voucherItemDto) {
        if (voucherItemDto != null) {
            voucherItem.setId(voucherItemDto.getId());
            voucherItem.setTitle(voucherItemDto.getTitle());
            if (voucherItemDto.getDebit() == null)
                voucherItem.setDebit(BigDecimal.ZERO);
            else
                voucherItem.setDebit(voucherItemDto.getDebit());
            if (voucherItemDto.getCredit() == null)
                voucherItem.setCredit(BigDecimal.ZERO);
            else
                voucherItem.setCredit(voucherItemDto.getCredit());
        }
        return voucherItem;
    }

    public TAccountDto setTAccount(TAccountDto tAccountDto, TAccount tAccount) {
        if (tAccount != null) {
            tAccountDto.setId(tAccount.getId());
            tAccountDto.setChildDigits(tAccount.getChildDigits());
            tAccountDto.setAccountCode(tAccount.getAccountCode());
            tAccountDto.setAccountNumber(tAccount.getAccountNumber());
            tAccountDto.setName(tAccount.getName());
            if (tAccount.getDescription() != null)
                tAccountDto.setDescription(tAccount.getDescription());
            tAccountDto.setIsCreditor(tAccount.isIsCreditor());
            tAccountDto.setIsDebtor(tAccount.isIsDebtor());
            tAccountDto.setIsActive(tAccount.isIsActive());
            tAccountDto.setAccountType(TAccountDto.AccountTypeEnum.valueOf(tAccount.getAccountType().name()));
            if (tAccount.getPartyCode() != null)
                tAccountDto.setPartyCode(tAccount.getPartyCode());
            tAccountDto.setParentId(tAccount.getAccount().getId());
//            if (tAccount.getAccount() != null)
//                tAccountDto.setTAccount(setTAccount(new TAccountDto(), tAccount.getAccount()));
            tAccountDto.setSysAccount(tAccount.isSysAccount());
        }
        return tAccountDto;
    }

    public TaxInfoDto setTaxInfo(TaxInfoDto taxInfoDto, TaxInfo taxInfo) {
        if (taxInfo != null) {
            taxInfoDto.setPartyId(taxInfo.getPartyId());
            taxInfoDto.setInvoiceType(InvoiceTypeDto.valueOf(taxInfo.getInvoiceType().name()));
            taxInfoDto.setServiceGroup(setServiceGroup(new ServiceGroupDto(), taxInfo.getServiceGroup()));
            taxInfoDto.setProductName(taxInfo.getProductName());
            taxInfoDto.setBillingDate(ir.boursam.accounting.service.util.DateUtil.convertFromGregorianToJalali(taxInfo.getBillingDate().toLocalDate()));
            taxInfoDto.setBillingNumber(taxInfo.getBillingNumber());
            taxInfoDto.setIncludedVAT(taxInfo.isIncludedVAT());
            taxInfoDto.setIncludedSeasonalTaxReport(taxInfo.isIncludedSeasonalTaxReport());
            taxInfoDto.setIncludedWithholdingTaxReport(taxInfo.isIncludedWithholdingTaxReport());
            taxInfoDto.setAction(ActionDto.UNCHANGED);
        }
        return taxInfoDto;
    }

    public TaxInfo setTaxInfo(TaxInfo taxInfo, TaxInfoDto taxInfoDto) {
        if (taxInfoDto != null) {
            taxInfo.setPartyId(taxInfoDto.getPartyId());
            taxInfo.setInvoiceType(InvoiceType.valueOf(taxInfoDto.getInvoiceType().name()));
            taxInfo.setProductName(taxInfoDto.getProductName());
            taxInfo.setBillingDate(ir.boursam.accounting.service.util.DateUtil.convertFromJalaliToGregorian(taxInfoDto.getBillingDate()).atStartOfDay().atZone(ZoneId.systemDefault()));
            taxInfo.setBillingNumber(taxInfoDto.getBillingNumber());
            taxInfo.setIncludedVAT(taxInfoDto.getIncludedVAT());
            taxInfo.setIncludedSeasonalTaxReport(taxInfoDto.getIncludedSeasonalTaxReport());
            taxInfo.setIncludedWithholdingTaxReport(taxInfoDto.getIncludedWithholdingTaxReport());
        }
        return taxInfo;
    }

    public ServiceGroupDto setServiceGroup(ServiceGroupDto serviceGroupDto, ServiceGroup serviceGroup) {
        if (serviceGroup != null) {
            serviceGroupDto.setId(serviceGroup.getId());
            serviceGroupDto.setTitle(serviceGroup.getTitle());
        }
        return serviceGroupDto;
    }

    public ServiceGroup setServiceGroup(ServiceGroup serviceGroup, ServiceGroupDto serviceGroupDto) {
        if (serviceGroupDto != null) {
            serviceGroup.setTitle(serviceGroupDto.getTitle());
        }
        return serviceGroup;
    }

    public List<VoucherAttachmentDto> setVoucherAttachments(List<VoucherAttachmentDto> voucherAttachmentDtos, Set<VoucherAttachment> voucherAttachments) {
        if (!voucherAttachments.isEmpty() && voucherAttachments.size() != 0 *//*&& !voucherAttachments != null*//*) {
            for (VoucherAttachment voucherAttachment : voucherAttachments)
                voucherAttachmentDtos.add(setVoucherAttachment(new VoucherAttachmentDto(), voucherAttachment));
        }
        return voucherAttachmentDtos;
    }

    public Set<VoucherAttachment> setVoucherAttachments(Set<VoucherAttachment> voucherAttachments, List<VoucherAttachmentDto> voucherAttachmentDtos) {
        if (!voucherAttachmentDtos.isEmpty() && voucherAttachmentDtos.size() != 0 *//*&& !voucherAttachmentDtos != null*//*) {
            for (VoucherAttachmentDto voucherAttachmentDto : voucherAttachmentDtos)
                voucherAttachments.add(setVoucherAttachment(new VoucherAttachment(), voucherAttachmentDto));
        }
        return voucherAttachments;
    }

    public VoucherAttachmentDto setVoucherAttachment(VoucherAttachmentDto voucherAttachmentDto, VoucherAttachment voucherAttachment) {
        if (voucherAttachment != null) {
            voucherAttachmentDto.setId(voucherAttachment.getId());
            voucherAttachmentDto.setDocumentId(voucherAttachment.getDocumnetId());
            if (voucherAttachment.getDescription() != null)
                voucherAttachmentDto.setDescription(voucherAttachment.getDescription());
            voucherAttachmentDto.setAction(ActionDto.UNCHANGED);
        }
        return voucherAttachmentDto;
    }

    public VoucherAttachment setVoucherAttachment(VoucherAttachment voucherAttachment, VoucherAttachmentDto voucherAttachmentDto) {
        if (voucherAttachmentDto != null) {
            voucherAttachment.setDocumnetId(voucherAttachmentDto.getDocumentId());
            voucherAttachment.setDescription(voucherAttachmentDto.getDescription());
        }
        return voucherAttachment;
    }

    public VoucherTypeDto setVoucherType(VoucherTypeDto voucherTypeDto, VoucherType voucherType) {
        if (voucherType != null) {
            voucherTypeDto.setId(voucherType.getId());
            voucherTypeDto.setTitle(voucherType.getTitle());
            voucherTypeDto.setVoucherCode(VoucherCodeDto.valueOf(voucherType.getCode().name()));
        }
        return voucherTypeDto;
    }

    public ResponceAutomaticVoucherDto setVoucherResponse(ResponceAutomaticVoucherDto responceAutomaticVoucherDto, Voucher voucher) {
        if (voucher != null) {
            responceAutomaticVoucherDto.setMessageCode("200-successfully");
            responceAutomaticVoucherDto.setVoucherCode(voucher.getCode());
            responceAutomaticVoucherDto.setVoucherId(voucher.getId());
        }else{
            responceAutomaticVoucherDto.setMessageCode("accountIsNotExistOrNotValid.");
            responceAutomaticVoucherDto.setVoucherCode("null");
        }
        return responceAutomaticVoucherDto;
    }

    public BlockedAccount setBlockedAccount(BlockedAccount blockedAccount, BlockedAccountDto blockedAccountDto) {
        blockedAccount.setPartyId(blockedAccountDto.getPartyId() != null ? blockedAccountDto.getPartyId() : null);
//        blockedAccount.setSequence(blockedAccountDto.getSequence() != null ? blockedAccountDto.getSequence() : null);
        blockedAccount.setBlockDate(blockedAccountDto.getBlockDate() != null ? ir.boursam.accounting.service.util.DateUtil.convertDateAndTimeFromJalaliToGregorian(blockedAccountDto.getBlockDate()) : null);
        blockedAccount.setBlockCause(blockedAccountDto.getBlockCause() != null ? BlockCause.valueOf(blockedAccountDto.getBlockCause().name()) : null);
//        blockedAccount.setRecordType(blockedAccountDto.getRecordType() != null ? blockedAccountDto.getRecordType() : null);
        blockedAccount.setRecordReferenceId(blockedAccountDto.getRecordReferenceId() != null ? blockedAccountDto.getRecordReferenceId() : null);
//        blockedAccount.setDebit(blockedAccountDto.getDebit() != null ? blockedAccountDto.getDebit() : null);
//        blockedAccount.setCredit(blockedAccountDto.getCredit() != null ? blockedAccountDto.getCredit() : null);
//        blockedAccount.setBlockedAmount(blockedAccountDto.getBlockedAmount() != null ? blockedAccountDto.getBlockedAmount() : null);
        blockedAccount.setDescription(blockedAccountDto.getDescription() != null ? blockedAccountDto.getDescription() : null);
        return blockedAccount;
    }

    public BlockedAccountDto setBlockedAccount(BlockedAccountDto blockedAccountDto, BlockedAccount blockedAccount) {
        blockedAccountDto.setId(blockedAccount.getId() != null ? blockedAccount.getId() : null);
        blockedAccountDto.setPartyId(blockedAccount.getPartyId() != null ? blockedAccount.getPartyId() : null);
        blockedAccountDto.setSequence(blockedAccount.getSequence() != null ? blockedAccount.getSequence() : null);
        blockedAccountDto.setBlockDate(blockedAccount.getBlockDate() != null ? ir.boursam.accounting.service.util.DateUtil.convertDateAndTimeFromGregorianToJalali(blockedAccount.getBlockDate()) : null);
        blockedAccountDto.setBlockCause(blockedAccount.getBlockCause() != null ? BlockCauseDto.valueOf(blockedAccount.getBlockCause().name()) : null);
        blockedAccountDto.setRecordType(blockedAccount.isRecordType() != null ? blockedAccount.isRecordType() : null);
        blockedAccountDto.setRecordReferenceId(blockedAccount.getRecordReferenceId() != null ? blockedAccount.getRecordReferenceId() : null);
        blockedAccountDto.setDebit(blockedAccount.getDebit() != null ? blockedAccount.getDebit() : null);
        blockedAccountDto.setCredit(blockedAccount.getCredit() != null ? blockedAccount.getCredit() : null);
        blockedAccountDto.setBlockedAmount(blockedAccount.getBlockedAmount() != null ? blockedAccount.getBlockedAmount() : null);
        blockedAccountDto.setDescription(blockedAccount.getDescription() != null ? blockedAccount.getDescription() : null);
        return blockedAccountDto;
    }

    public BankAccountDto setBankAccount(BankAccountDto bankAccountDto, BankAccount bankAccount) {

        bankAccountDto.setId(bankAccount.getId() != null ? bankAccount.getId() : null);
        bankAccountDto.setAccountTitle(bankAccount.getTitle() != null ? bankAccount.getTitle() : null);
        bankAccountDto.setAccountNumber(bankAccount.getAccountNumber() != null ? bankAccount.getAccountNumber() : null);
        bankAccountDto.setShabaNumber(bankAccount.getShabaNumber() != null ? bankAccount.getShabaNumber() : null);
        bankAccountDto.setCashAccount(bankAccount.isIsCash() != null ? bankAccount.isIsCash() : null);
        bankAccountDto.setGatewayAccount(bankAccount.isIsGateway() != null ? bankAccount.isIsGateway() : null);
        bankAccountDto.setActive(bankAccount.isIsActive() != null ? bankAccount.isIsActive() : null);
        bankAccountDto.setBankId(bankAccount.getBank() != null ? bankAccount.getBank().longValue() : null);
        return bankAccountDto;
    }

    public PurchasingPowerAccount setPurchasingPoweredAccount(PurchasingPowerAccount purchasingPowerAccount, PurchasingPowerAccountDto purchasingPowerAccountDto) {
        purchasingPowerAccount.setPartyId(purchasingPowerAccountDto.getPartyId() != null ? purchasingPowerAccountDto.getPartyId() : null);
//        purchasingPowerAccount.setSequence(purchasingPowerAccountDto.getSequence() != null ? purchasingPowerAccountDto.getSequence() : null);
        purchasingPowerAccount.setPowerDate(purchasingPowerAccountDto.getPowerDate() != null ? ir.boursam.accounting.service.util.DateUtil.convertDateAndTimeFromJalaliToGregorian(purchasingPowerAccountDto.getPowerDate()) : null);
//        purchasingPowerAccount.setRecordType(purchasingPowerAccountDto.getRecordType() != null ? purchasingPowerAccountDto.getRecordType() : null);
        purchasingPowerAccount.setRecordReferenceId(purchasingPowerAccountDto.getRecordReferenceId() != null ? purchasingPowerAccountDto.getRecordReferenceId() : null);
//        purchasingPowerAccount.setDebit(purchasingPowerAccountDto.getDebit() != null ? purchasingPowerAccountDto.getDebit() : null);
//        purchasingPowerAccount.setCredit(purchasingPowerAccountDto.getCredit() != null ? purchasingPowerAccountDto.getCredit() : null);
//        purchasingPowerAccount.setBlockedAmount(purchasingPowerAccountDto.getBlockedAmount() != null ? purchasingPowerAccountDto.getBlockedAmount() : null);
        purchasingPowerAccount.setDescription(purchasingPowerAccountDto.getDescription() != null ? purchasingPowerAccountDto.getDescription() : null);
        return purchasingPowerAccount;
    }

    public PurchasingPowerAccountDto setPurchasingPoweredAccount(PurchasingPowerAccountDto purchasingPowerAccountDto, PurchasingPowerAccount purchasingPowerAccount) {
        purchasingPowerAccountDto.setId(purchasingPowerAccount.getId() != null ? purchasingPowerAccount.getId() : null);
        purchasingPowerAccountDto.setPartyId(purchasingPowerAccount.getPartyId() != null ? purchasingPowerAccount.getPartyId() : null);
        purchasingPowerAccountDto.setSequence(purchasingPowerAccount.getSequence() != null ? purchasingPowerAccount.getSequence() : null);
        purchasingPowerAccountDto.setPowerDate(purchasingPowerAccount.getPowerDate() != null ? ir.boursam.accounting.service.util.DateUtil.convertDateAndTimeFromGregorianToJalali(purchasingPowerAccount.getPowerDate()) : null);
        purchasingPowerAccountDto.setRecordType(purchasingPowerAccount.isRecordType() != null ? purchasingPowerAccount.isRecordType() : null);
        purchasingPowerAccountDto.setRecordReferenceId(purchasingPowerAccount.getRecordReferenceId() != null ? purchasingPowerAccount.getRecordReferenceId() : null);
        purchasingPowerAccountDto.setDebit(purchasingPowerAccount.getDebit() != null ? purchasingPowerAccount.getDebit() : null);
        purchasingPowerAccountDto.setCredit(purchasingPowerAccount.getCredit() != null ? purchasingPowerAccount.getCredit() : null);
        purchasingPowerAccountDto.setPoweredAmount(purchasingPowerAccount.getPoweredAmount() != null ? purchasingPowerAccount.getPoweredAmount() : null);
        purchasingPowerAccountDto.setDescription(purchasingPowerAccount.getDescription() != null ? purchasingPowerAccount.getDescription() : null);
        return purchasingPowerAccountDto;
    }*/
}
