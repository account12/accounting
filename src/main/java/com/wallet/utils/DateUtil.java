package com.wallet.utils;

import net.time4j.PlainDate;
import net.time4j.calendar.PersianCalendar;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class DateUtil {

    public static String convertFromGregorianToJalali(LocalDate date) {
        String[] dateArray = date.toString().split("-");
        PlainDate plainDate = PlainDate.from(PlainDate.of(Integer.valueOf(dateArray[0]), Integer.valueOf(dateArray[1]), Integer.valueOf(dateArray[2])));
        PersianCalendar persianCalendar = plainDate.transform(PersianCalendar.class);
        return persianCalendar.toString().substring(3).replace("-", "/");
    }

    public static LocalDate convertFromJalaliToGregorian(String date) {
        String[] dateArray = date.split("/");
        PersianCalendar persianCalendar = PersianCalendar.of(Integer.valueOf(dateArray[0]), Integer.valueOf(dateArray[1]), Integer.valueOf(dateArray[2]));
        return LocalDate.parse(persianCalendar.transform(PlainDate.class).toString());
    }

    public static String convertDateAndTimeFromGregorianToJalali(ZonedDateTime dateTime) {
        String[] dateArray = dateTime.toLocalDate().toString().split("-");
        String time = dateTime.toLocalTime().toString();
        PlainDate plainDate = PlainDate.from(PlainDate.of(Integer.valueOf(dateArray[0]), Integer.valueOf(dateArray[1]), Integer.valueOf(dateArray[2])));
        PersianCalendar persianCalendar = plainDate.transform(PersianCalendar.class);
        return persianCalendar.toString().substring(3).replace("-", "/") + " " + time;
    }

    public static ZonedDateTime convertDateAndTimeFromJalaliToGregorian(String dateTime) {
        String[] dateArray = dateTime.split(" ")[0].split("/");
        String time = dateTime.split(" ")[1];
        PersianCalendar persianCalendar = PersianCalendar.of(Integer.valueOf(dateArray[0]), Integer.valueOf(dateArray[1]), Integer.valueOf(dateArray[2]));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return ZonedDateTime.of(LocalDateTime.parse(persianCalendar.transform(PlainDate.class).toString() + " " +time, formatter), ZoneId.systemDefault());
    }
}
