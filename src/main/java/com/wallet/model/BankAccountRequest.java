package com.wallet.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wallet.utils.EducationGradeEnum;
import com.wallet.utils.PartyGenderEnum;
import com.wallet.utils.PartyStateEnum;
import com.wallet.utils.enums.CurrencyTypeEnum;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by T.Ranjbar on 7/27/2020.
 */

public class BankAccountRequest implements Serializable {

    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("accountCode")
    private String accountCode = null;

    @JsonProperty("accountNumber")
    private String accountNumber = null;

    @JsonProperty("description")
    private String description = null;

    @JsonProperty("isCreditor")
    private Boolean isCreditor = null;

    @JsonProperty("isDebtor")
    private Boolean isDebtor = null;

    @JsonProperty("isActive")
    private Boolean isActive = null;

    @JsonProperty("firstName")
    private String firstName;

    @JsonProperty("family")
    private String family;

    @JsonProperty("gender")
    private PartyGenderEnum gender;

    @JsonProperty("username")
    private String username;

    @JsonProperty("password")
    private String password;

    @JsonProperty("email")
    private String email;

    @JsonProperty("mobile")
    private String mobile;

    @JsonProperty("birthDate")
    private Date birthDate;

    @JsonProperty("partyState")
    private PartyStateEnum partyState;

    @JsonProperty("postCode")
    private String postCode;

    @JsonProperty("activationCode")
    private String activationCode;

    @JsonProperty("educationGrade")
    private EducationGradeEnum educationGrade;

    @JsonProperty("title")
    private String title;

    @JsonProperty("shabaNumber")
    private String shabaNumber;

    @JsonProperty("bank")
    private Integer bank;

    @JsonProperty("isCash")
    private Boolean isCash;

    @JsonProperty("currencyName")
    private CurrencyTypeEnum currencyName;

    @JsonProperty("partyCode")
    private Long partyCode = null;

    @JsonProperty("amount")
    private Double amount;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public PartyGenderEnum getGender() {
        return gender;
    }

    public void setGender(PartyGenderEnum gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public PartyStateEnum getPartyState() {
        return partyState;
    }

    public void setPartyState(PartyStateEnum partyState) {
        this.partyState = partyState;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public EducationGradeEnum getEducationGrade() {
        return educationGrade;
    }

    public void setEducationGrade(EducationGradeEnum educationGrade) {
        this.educationGrade = educationGrade;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getShabaNumber() {
        return shabaNumber;
    }

    public void setShabaNumber(String shabaNumber) {
        this.shabaNumber = shabaNumber;
    }

    public Integer getBank() {
        return bank;
    }

    public void setBank(Integer bank) {
        this.bank = bank;
    }

    public Boolean getCash() {
        return isCash;
    }

    public void setCash(Boolean cash) {
        isCash = cash;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public CurrencyTypeEnum getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(CurrencyTypeEnum currencyName) {
        this.currencyName = currencyName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCreditor() {
        return isCreditor;
    }

    public void setCreditor(Boolean creditor) {
        isCreditor = creditor;
    }

    public Boolean getDebtor() {
        return isDebtor;
    }

    public void setDebtor(Boolean debtor) {
        isDebtor = debtor;
    }

    public Long getPartyCode() {
        return partyCode;
    }

    public void setPartyCode(Long partyCode) {
        this.partyCode = partyCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "BankAccountRequest{" +
                "id=" + id +
                ", accountCode='" + accountCode + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", description='" + description + '\'' +
                ", isCreditor=" + isCreditor +
                ", isDebtor=" + isDebtor +
                ", isActive=" + isActive +
                ", firstName='" + firstName + '\'' +
                ", family='" + family + '\'' +
                ", gender=" + gender +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                ", birthDate=" + birthDate +
                ", partyState=" + partyState +
                ", postCode='" + postCode + '\'' +
                ", activationCode='" + activationCode + '\'' +
                ", educationGrade=" + educationGrade +
                ", title='" + title + '\'' +
                ", shabaNumber='" + shabaNumber + '\'' +
                ", bank=" + bank +
                ", isCash=" + isCash +
                ", currencyName=" + currencyName +
                ", partyCode=" + partyCode +
                ", amount=" + amount +
                '}';
    }
}
