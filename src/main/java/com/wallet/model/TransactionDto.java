package com.wallet.model;

import com.wallet.utils.enums.TransactionTypeEnum;

import java.io.Serializable;

/**
 * Created by taraneh on 8/1/2020.
 */
public class TransactionDto implements Serializable {

    String msisdn;
    Double amount;
    String desc;
    String refId;
    TransactionTypeEnum transactionTypeEnum;
    Long currencyId;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public TransactionTypeEnum getTransactionTypeEnum() {
        return transactionTypeEnum;
    }

    public void setTransactionTypeEnum(TransactionTypeEnum transactionTypeEnum) {
        this.transactionTypeEnum = transactionTypeEnum;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    @Override
    public String toString() {
        return "TransactionDto{" +
                "msisdn='" + msisdn + '\'' +
                ", amount=" + amount +
                ", desc='" + desc + '\'' +
                ", refId='" + refId + '\'' +
                ", transactionTypeEnum=" + transactionTypeEnum +
                ", currencyId=" + currencyId +
                '}';
    }
}
