package com.wallet.model;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * Created by taraneh on 7/24/2020.
 */
public class CurrencyResponse implements Serializable {

    private HttpStatus responseType;
    private String responseDesc;

    public HttpStatus getResponseType() {
        return responseType;
    }

    public void setResponseType(HttpStatus responseType) {
        this.responseType = responseType;
    }

    public String getResponseDesc() {
        return responseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        this.responseDesc = responseDesc;
    }

    @Override
    public String toString() {
        return "CurrencyResponse{" +
                "responseType='" + responseType + '\'' +
                ", responseDesc='" + responseDesc + '\'' +
                '}';
    }
}
