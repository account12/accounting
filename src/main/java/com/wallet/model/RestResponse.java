package com.wallet.model;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class RestResponse<T> extends ResponseEntity<T> {

	public RestResponse(T body, HttpStatus status) {
		super(body, status);
	}

}
