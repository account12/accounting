package com.wallet.model;

import java.io.Serializable;

/**
 * Created by taraneh on 8/1/2020.
 */
public class TransferDto implements Serializable {
    Long firstAccountId ;
    Double amount ;
    Long secondAccountId ;
    String desc ;
    String refId;

    public Long getFirstAccountId() {
        return firstAccountId;
    }

    public void setFirstAccountId(Long firstAccountId) {
        this.firstAccountId = firstAccountId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getSecondAccountId() {
        return secondAccountId;
    }

    public void setSecondAccountId(Long secondAccountId) {
        this.secondAccountId = secondAccountId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    @Override
    public String toString() {
        return "TransferDto{" +
                "firstAccountId=" + firstAccountId +
                ", amount=" + amount +
                ", secondAccountId=" + secondAccountId +
                ", desc='" + desc + '\'' +
                ", refId='" + refId + '\'' +
                '}';
    }
}
