package com.wallet.model;

import com.wallet.utils.enums.CurrencyTypeEnum;

import java.io.Serializable;

public class DebitDto implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private CurrencyTypeEnum currencyEnum;

	private Long creditAccount;
	
	private Long debitAccount;
	
	private Double amount;

	private String debitDesc;
	
	private String referCode;
	
	private Long reqId;

	public CurrencyTypeEnum getCurrencyEnum() {
		return currencyEnum;
	}

	public void setCurrencyEnum(CurrencyTypeEnum currencyEnum) {
		this.currencyEnum = currencyEnum;
	}

	public Long getCreditAccount() {
		return creditAccount;
	}

	public void setCreditAccount(Long creditAccount) {
		this.creditAccount = creditAccount;
	}

	public Long getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(Long debitAccount) {
		this.debitAccount = debitAccount;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getDebitDesc() {
		return debitDesc;
	}

	public void setDebitDesc(String debitDesc) {
		this.debitDesc = debitDesc;
	}

	public String getReferCode() {
		return referCode;
	}

	public void setReferCode(String referCode) {
		this.referCode = referCode;
	}

	public Long getReqId() {
		return reqId;
	}

	public void setReqId(Long reqId) {
		this.reqId = reqId;
	}

	@Override
	public String toString() {
		return "DebitDto{" +
				"currencyEnum=" + currencyEnum +
				", creditAccount=" + creditAccount +
				", debitAccount=" + debitAccount +
				", amount=" + amount +
				", debitDesc='" + debitDesc + '\'' +
				", referCode='" + referCode + '\'' +
				", reqId=" + reqId +
				'}';
	}
}