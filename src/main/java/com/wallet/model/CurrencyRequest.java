package com.wallet.model;

import com.wallet.utils.enums.CurrencyTypeEnum;

import java.io.Serializable;

/**
 * Created by taraneh on 7/24/2020.
 */
public class CurrencyRequest implements Serializable {

    private Long id;
    private CurrencyTypeEnum name;

    public CurrencyTypeEnum getName() {
        return name;
    }

    public void setName(CurrencyTypeEnum name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "CurrencyRequest{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}
