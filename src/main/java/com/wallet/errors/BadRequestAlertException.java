package com.wallet.errors;



import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class BadRequestAlertException {

    private final String entityName;

    private final String errorKey;


    public BadRequestAlertException( String entityName, String errorKey) {
        this.entityName = entityName;
        this.errorKey = errorKey;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getErrorKey() {
        return errorKey;
    }

    private static Map<String, Object> getAlertParameters(String entityName, String errorKey) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("message", "error." + errorKey);
        parameters.put("params", entityName);
        return parameters;
    }
}
