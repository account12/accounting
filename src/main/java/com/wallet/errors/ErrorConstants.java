package com.wallet.errors;

public final class ErrorConstants {


    private ErrorConstants() {
    }

    public static final String ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure";
    public static final String ERR_VALIDATION = "error.validation";


    public static final String  Currency_Insert_Failed_Exception = "CurrencyInsertFailedException";
    public static final String  Currency_Is_Already_Exception = "CurrencyIsAlreadyException";
    public static final String  Account_Code_IsBlank_Exception = "AccountCodeIsBlankException";
    public static final String  Password_Encrypt_Exception = "PasswordEncryptException";
    public static final String  Account_Not_Found_Exception = "AccountNotFoundException";
    public static final String  Party_Not_Found_Exception = "PartyNotFoundException";
    public static final String  Currency_Not_Found_Exception = "CurrencyNotFoundException";
    public static final String  Invalid_Debit_Amount_Exception = "InvalidDebitAmountException";





}
