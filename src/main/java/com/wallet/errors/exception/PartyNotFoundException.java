package com.wallet.errors.exception;

/**
 * Created by taraneh on 7/24/2020.
 */
public class PartyNotFoundException extends RuntimeException{
    public PartyNotFoundException(String message) {
        super(message);
    }
}
