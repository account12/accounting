package com.wallet.errors.exception;

    import org.slf4j.helpers.MessageFormatter;

public class InvalidDebitAmountException extends RuntimeException {
    private static final long serialVersionUID = -8757219816102233453L;

    public InvalidDebitAmountException(String message, Object... args) {
        super(MessageFormatter.arrayFormat(message, args).getMessage());
    }

    public InvalidDebitAmountException(Throwable cause, String message, Object... args) {
        super(MessageFormatter.arrayFormat(message, args).getMessage(), cause);
    }


}
