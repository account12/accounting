package com.wallet.errors.exception;

    import org.slf4j.helpers.MessageFormatter;

public class AccountCodeMustBeNumberOnlyException extends RuntimeException {
    private static final long serialVersionUID = -8757219816102233453L;

    public AccountCodeMustBeNumberOnlyException(String message, Object... args) {
        super(MessageFormatter.arrayFormat(message, args).getMessage());
    }

    public AccountCodeMustBeNumberOnlyException(Throwable cause, String message, Object... args) {
        super(MessageFormatter.arrayFormat(message, args).getMessage(), cause);
    }


}
