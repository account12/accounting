package com.wallet.errors.exception;

/**
 * Created by taraneh on 7/24/2020.
 */
public class CurrencyIsAlreadyException extends RuntimeException{
    public CurrencyIsAlreadyException(String message) {
        super(message);
    }
}
