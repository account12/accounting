package com.wallet.errors.exception;

    import org.slf4j.helpers.MessageFormatter;

public class AccountBalanceNotEnoughException extends RuntimeException {
    private static final long serialVersionUID = -8757219816102233453L;

    public AccountBalanceNotEnoughException(String message, Object... args) {
        super(MessageFormatter.arrayFormat(message, args).getMessage());
    }

    public AccountBalanceNotEnoughException(Throwable cause, String message, Object... args) {
        super(MessageFormatter.arrayFormat(message, args).getMessage(), cause);
    }


}
