package com.wallet.errors.exception;

/**
 * Created by T.Ranjbar on 7/29/2020.
 */
public class AccountCodeIsBlankException extends RuntimeException{
    public AccountCodeIsBlankException(String message) {
        super(message);
    }
}
