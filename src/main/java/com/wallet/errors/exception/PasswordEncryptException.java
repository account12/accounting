package com.wallet.errors.exception;

public class PasswordEncryptException extends RuntimeException{
    public PasswordEncryptException(String message) {
        super(message);
    }
}
