package com.wallet.errors.exception;

    import org.slf4j.helpers.MessageFormatter;

public class InvalidAmountException extends RuntimeException {
    private static final long serialVersionUID = -8757219816102233453L;

    public InvalidAmountException(String message, Object... args) {
        super(MessageFormatter.arrayFormat(message, args).getMessage());
    }

    public InvalidAmountException(Throwable cause, String message, Object... args) {
        super(MessageFormatter.arrayFormat(message, args).getMessage(), cause);
    }


}
