package com.wallet.errors.exception;

/**
 * Created by taraneh on 7/24/2020.
 */
public class CurrencyNotFoundException extends RuntimeException{
    public CurrencyNotFoundException(String message) {
        super(message);
    }
}
