package com.wallet.errors.exception;

public class CurrencyInsertFailedException extends RuntimeException{
    public CurrencyInsertFailedException(String message) {
        super(message);
    }
}
