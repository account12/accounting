package com.wallet.service;

import com.wallet.utils.enums.CurrencyTypeEnum;

/**
 * Created by taraneh on 7/31/2020.
 */
public interface ExchangeRateService {

    Double exchangeDollarToRial(CurrencyTypeEnum currency, Double amount);
    Double exchangeRialToDollar(CurrencyTypeEnum currency, Double amount);

}
