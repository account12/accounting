package com.wallet.service;

import com.wallet.entity.Document;
import com.wallet.entity.Transaction;
import com.wallet.entity.TransactionType;
import com.wallet.utils.enums.TransactionTypeEnum;

/**
 * Created by taraneh on 7/31/2020.
 */
public interface TransactionService {

    Long getBalance(Long accountId,Long currencyId);
    TransactionType findById(Long id);
    Transaction bookTransaction(Document doc, Long accountId, Double Amount, Long currencyId, TransactionTypeEnum transactionTypeEnum, String trnDesc);

    }
