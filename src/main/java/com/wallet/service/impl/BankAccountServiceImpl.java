package com.wallet.service.impl;

import com.wallet.entity.BankAccount;
import com.wallet.entity.Currency;
import com.wallet.entity.Party;
import com.wallet.errors.ErrorConstants;
import com.wallet.errors.exception.*;
import com.wallet.model.BankAccountRequest;
import com.wallet.service.BankAccountService;
import com.wallet.service.repository.BankAccountRepository;
import com.wallet.service.repository.CurrencyRepository;
import com.wallet.service.repository.PartyRepository;
import com.wallet.utils.EncryptUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by T.Ranjbar on 7/27/2020.
 */

@Service
@Transactional
public class BankAccountServiceImpl implements BankAccountService{

    @Autowired
    private PartyRepository partyRepository;
    @Autowired
    private BankAccountRepository bankAccountRepository;
    @Autowired
    private CurrencyRepository currencyRepository;

    @Override
    public void createBankAccount(BankAccountRequest bankAccountRequest) {

        if (bankAccountRequest.getAccountCode().length() == 0)
            throw new AccountCodeIsBlankException(ErrorConstants.Account_Code_IsBlank_Exception);

        if (!bankAccountRequest.getAccountCode().matches("[0-9]+"))
            throw new AccountCodeMustBeNumberOnlyException("AccountCode={} is not Number only", bankAccountRequest.getAccountCode());

        Party party = new Party();
        party.setFirstName(bankAccountRequest.getFirstName());
        party.setFamily(bankAccountRequest.getFamily());
        party.setActivationCode(bankAccountRequest.getActivationCode());
        party.setEmail(bankAccountRequest.getEmail());
        party.setMobile(bankAccountRequest.getMobile());
        party.setPostCode(bankAccountRequest.getPostCode());
        party.setBirthDate(bankAccountRequest.getBirthDate());
        party.setEducationGrade(bankAccountRequest.getEducationGrade());
        party.setPartyState(bankAccountRequest.getPartyState());
        party.setGender(bankAccountRequest.getGender());
        party.setUsername(bankAccountRequest.getUsername());
        try {
            party.setPassword(EncryptUtil.encrypt(bankAccountRequest.getPassword()));
        } catch (Exception e) {
          throw new PasswordEncryptException(ErrorConstants.Password_Encrypt_Exception);
        }
        partyRepository.save(party);
        BankAccount account = new BankAccount();
        account.setParty(party);
        account.setAccountNumber(bankAccountRequest.getAccountNumber());
        account.setActive(bankAccountRequest.getActive());
        account.setBank(bankAccountRequest.getBank());
        account.setCash(bankAccountRequest.getCash());
        account.setShabaNumber(bankAccountRequest.getShabaNumber());
        account.setTitle(bankAccountRequest.getTitle());
        account.setBalance(bankAccountRequest.getAmount());
        Currency currency = currencyRepository.findByName(bankAccountRequest.getCurrencyName().name());
        account.setCurrency(currency);
        bankAccountRepository.save(account);
    }

    @Override
    public void updateBankAccount(BankAccountRequest bankAccountRequest) {
        BankAccount account = bankAccountRepository.findByAccountCode(bankAccountRequest.getAccountCode());
        if(account == null){
            throw  new AccountNotFoundException(ErrorConstants.Account_Not_Found_Exception);
        }
        if(account.getParty() == null){
            throw  new PartyNotFoundException(ErrorConstants.Party_Not_Found_Exception);
        }
        if(bankAccountRequest.getAccountCode()!=null){
            if (!bankAccountRequest.getAccountCode().matches("[0-9]+")) {
                throw new AccountCodeMustBeNumberOnlyException("AccountCode={} is not Number only", bankAccountRequest.getAccountCode());
            }
            account.setAccountCode(bankAccountRequest.getAccountCode());
        }if(bankAccountRequest.getBank()!=null){
            account.setBank(bankAccountRequest.getBank());
        }if(bankAccountRequest.getShabaNumber()!=null){
            account.setShabaNumber(bankAccountRequest.getShabaNumber());
        }if(bankAccountRequest.getActive()!=null){
            account.setActive(bankAccountRequest.getActive());
        }if(bankAccountRequest.getTitle()!=null){
            account.setTitle(bankAccountRequest.getTitle());
        }if(bankAccountRequest.getAmount() !=null){
            account.setBalance(bankAccountRequest.getAmount());
        }
        Currency currency  = account.getCurrency();
        if(currency == null){
            throw new CurrencyNotFoundException(ErrorConstants.Currency_Not_Found_Exception);
        }
        if(bankAccountRequest.getCurrencyName() != null &&  !currency.getName().equals(bankAccountRequest.getCurrencyName())){
            account.getCurrency().setName(bankAccountRequest.getCurrencyName());
        }
        bankAccountRepository.save(account);
    }

    @Override
    public BankAccount findByMobile(String msisdn){
     return  bankAccountRepository.findByMobile(msisdn);
    }

    @Override
    public BankAccount finById(Long id) {
        return bankAccountRepository.getOne(id);
    }


}
