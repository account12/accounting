package com.wallet.service.impl;

import com.wallet.entity.BankAccount;
import com.wallet.entity.Document;
import com.wallet.entity.Party;
import com.wallet.entity.Transaction;
import com.wallet.errors.ErrorConstants;
import com.wallet.errors.exception.InvalidDebitAmountException;
import com.wallet.model.DebitDto;
import com.wallet.model.TransactionDto;
import com.wallet.model.TransferDto;
import com.wallet.service.*;
import com.wallet.utils.enums.CurrencyTypeEnum;
import com.wallet.utils.enums.DocumentTypeEnum;
import com.wallet.utils.enums.TransactionTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@EnableAsync
@Transactional
public class WalletServiceImpl implements WalletService {

    private static final Logger logger = LoggerFactory.getLogger(WalletServiceImpl.class);


    @Autowired
    private DocumentService documentService;
    @Autowired
    private BankAccountService bankAccountService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private UtilService utilService;
    @Autowired
    private CurrencyService currencyService;
    @Autowired
    private ExchangeRateService exchangeRateService;
    @Value("${currency.rial}")
    private Long rial;


    @Override
    public Long credit(TransactionDto dto) {
        boolean isOk = utilService.validateRefid(dto.getRefId());
        dto.setMsisdn(utilService.fixMsisdn(dto.getMsisdn()));

        BankAccount account = bankAccountService.findByMobile(dto.getMsisdn().trim());
        /*if (account == null) {
            logger.info("no Account then create account....");
			account = createAccount(sid, msisdn);
		}*/
        Double rate = null;
        if (account.getCurrency().getId() != dto.getCurrencyId()) {
            rate = getRate(dto.getCurrencyId(), dto.getAmount());
        }

        if (!isOk) {
            Long balance = transactionService.getBalance(account.getId(), account.getCurrency().getId());
            if (balance != null) {
                return balance;
            } else {
                return -1L;
            }
        }

        DebitDto debitDto = new DebitDto();
        if (rate != null) {
            dto.setAmount(rate);
        }
        debitDto.setAmount(dto.getAmount());
        debitDto.setDebitDesc(dto.getDesc());
        debitDto.setReferCode(dto.getRefId());
        Document debitDocument = documentService.getDocument(debitDto, DocumentTypeEnum.CREDIT);

        Transaction transaction = transactionService.bookTransaction(debitDocument, account.getId(), debitDto.getAmount(),
                dto.getTransactionTypeEnum().getId(), TransactionTypeEnum.Credit, debitDto.getDebitDesc());
        Long balance = 0L;
        if (transaction != null) {
            balance = transactionService.getBalance(account.getId(), account.getCurrency().getId());
            if (balance != null) {
                return balance;
            }
            return 0L;
        }
        return balance;
    }

    @Override
    public Long debit(TransactionDto dto) {
        String mobile = utilService.fixMsisdn(dto.getMsisdn());
        BankAccount account = bankAccountService.findByMobile(mobile.trim());

        Double rate = null;
        if (account.getCurrency().getId() != dto.getCurrencyId()) {
            rate = getRate(dto.getCurrencyId(), dto.getAmount());
        }

        Long balance = transactionService.getBalance(account.getId(), account.getCurrency().getId());
        if (balance == null) {
            balance = 0L;
        }

        if (rate != null) {
            dto.setAmount(rate);
        }

        synchronized (balance) {
            if (balance < dto.getAmount()) {
                throw new InvalidDebitAmountException(ErrorConstants.Invalid_Debit_Amount_Exception);
            } else {
                DebitDto debitDto = new DebitDto();
                debitDto.setAmount(dto.getAmount());
                debitDto.setDebitDesc(dto.getDesc());
                debitDto.setReferCode(dto.getRefId());

                Document debitDocument = documentService.getDocument(debitDto, DocumentTypeEnum.DEBIT);

                Transaction transaction = transactionService.bookTransaction(debitDocument, account.getId(),
                        debitDto.getAmount(), dto.getTransactionTypeEnum().getId(), TransactionTypeEnum.Debit, debitDto.getDebitDesc());

                if (transaction != null) {
                    balance = transactionService.getBalance(account.getId(), account.getCurrency().getId());
                    if (balance == null) {
                        balance = 0L;
                    }
                }
            }
        }
        return balance;
    }


    @Override
    public Long transfer(TransferDto dto) {
        BankAccount account1 = bankAccountService.finById(dto.getFirstAccountId());
        if (account1 == null) {
            //throw new exc
        }
        Party party1 = account1.getParty();
        if (party1 == null) {
            //throw new exc
        }
        BankAccount account2 = bankAccountService.finById(dto.getSecondAccountId());
        if (account2 == null) {
            //throw new exc
        }
        Party party2 = account2.getParty();
        if (party2 == null) {
            //throw new exc
        }
        Double rate = null;
        if (!account1.getCurrency().equals(account2.getCurrency())) {

            if (account1.getCurrency().getName().name().equals(CurrencyTypeEnum.RIAL) && account2.getCurrency().getName().name().equals(CurrencyTypeEnum.DOLLAR.getValue())) {
                rate = exchangeRateService.exchangeRialToDollar(CurrencyTypeEnum.RIAL, dto.getAmount());
            } else if (account1.getCurrency().getName().name().equals(CurrencyTypeEnum.DOLLAR) && account2.getCurrency().getName().name().equals(CurrencyTypeEnum.RIAL.getValue())) {
                rate = exchangeRateService.exchangeDollarToRial(CurrencyTypeEnum.DOLLAR, dto.getAmount());
            }
        }
        if (rate != null) {
            dto.setAmount(rate);
        }
        Long balance = transactionService.getBalance(account1.getId(), account1.getCurrency().getId());
        if (balance == null) {
            balance = 0L;
        }

        synchronized (balance) {
            DebitDto debitDto = new DebitDto();
            if (balance < dto.getAmount()) {
                throw new InvalidDebitAmountException(ErrorConstants.Invalid_Debit_Amount_Exception);
            } else {

                debitDto.setAmount(dto.getAmount());
                debitDto.setDebitDesc(dto.getDesc());
                debitDto.setReferCode(dto.getRefId());

                Document debitDocument = documentService.getDocument(debitDto, DocumentTypeEnum.DEBIT);

                Transaction debitTransaction = transactionService.bookTransaction(debitDocument, account1.getId(),
                        debitDto.getAmount(), TransactionTypeEnum.Debit.getId(), TransactionTypeEnum.Debit, debitDto.getDebitDesc());
            }

            Document creditDocument = documentService.getDocument(debitDto, DocumentTypeEnum.CREDIT);

            Transaction creditTransaction = transactionService.bookTransaction(creditDocument, account2.getId(), debitDto.getAmount(),
                    TransactionTypeEnum.Credit.getId(), TransactionTypeEnum.Credit, debitDto.getDebitDesc());

            balance = transactionService.getBalance(account2.getId(), account2.getCurrency().getId());
            if (balance != null) {
                return balance;
            }
        }

        return balance;
    }

    private Double getRate(Long currencyId, Double amount) {
        String currentCurrency = currencyService.findByName(currencyService.findById(currencyId).getName());
        if (currentCurrency.equals(CurrencyTypeEnum.DOLLAR)) {
            return exchangeRateService.exchangeDollarToRial(CurrencyTypeEnum.DOLLAR, amount);
        } else {
            return exchangeRateService.exchangeRialToDollar(CurrencyTypeEnum.RIAL, amount);
        }
    }


}
