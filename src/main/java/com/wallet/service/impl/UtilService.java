package com.wallet.service.impl;

import com.wallet.entity.Document;
import com.wallet.service.DocumentService;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by taraneh on 7/31/2020.
 */

@Component
public class UtilService {

    @Autowired
    private DocumentService documentService;

    public boolean validateRefid(String refid) {
        if (refid == null || refid.equalsIgnoreCase("0")) {
            return true;
        }
        List<Document> lstDoc = documentService.findBydocref(refid);
        if ((lstDoc == null) || (lstDoc.size() < 1)) {
            return true;
        }
        return false;
    }
    public String fixMsisdn(String msisdn) throws ServiceException {
        if ((msisdn == null) || (msisdn.length() < 10) || (msisdn.length() >11))  {
            throw new ServiceException("Invlaid MSISDN");
        }
        if (msisdn.startsWith("9")) {
            msisdn = "0" + msisdn.substring(0, 10);
        }
        if (msisdn.startsWith("09") && msisdn.length() >= 11) {
            return msisdn.substring(0, 11);
        }
        if (msisdn.startsWith("98") || msisdn.startsWith("+98")) {
            if (msisdn.trim().length() == 12) {
                msisdn = "0" + msisdn.substring(2, msisdn.length());
            } else {
                if (msisdn.trim().length() == 13) {
                    msisdn = "0" + msisdn.substring(3, msisdn.length());
                }
            }

        }
        return msisdn;
    }


}
