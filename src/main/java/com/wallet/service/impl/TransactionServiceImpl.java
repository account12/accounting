package com.wallet.service.impl;

import com.wallet.entity.*;
import com.wallet.service.BankAccountService;
import com.wallet.service.CurrencyService;
import com.wallet.service.TransactionService;
import com.wallet.service.repository.TransactionRepository;
import com.wallet.service.repository.TransactionTypeRepository;
import com.wallet.utils.enums.TransactionTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by taraneh on 7/31/2020.
 */

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private TransactionTypeRepository transactionTypeRepository;
    @Autowired
    private BankAccountService bankAccountService;
    @Autowired
    private CurrencyService currencyService;

    @Override
    public Long getBalance(Long accountId,Long currencyId) {
        return transactionRepository.getBalance(accountId,currencyId);
    }

    @Override
    public TransactionType findById(Long id) {
        return transactionTypeRepository.getOne(id);
    }

    @Override
    public Transaction bookTransaction(Document doc, Long accountId, Double Amount, Long currencyId, TransactionTypeEnum transactionTypeEnum, String trnDesc) {
        Transaction transaction = new Transaction();

        TransactionType transactionType = transactionTypeRepository.getOne(transactionTypeEnum.getId());

        if (transactionType != null) {
            BankAccount account = bankAccountService.finById(accountId);
            transaction.setAccount(account);

            transaction.setTransactionType(transactionType);

            transaction.setDocument(doc);
            if (transactionTypeEnum.equals(TransactionTypeEnum.Credit)) {
                transaction.setCreditAmount(Amount);
                transaction.setDebitAmount(0.0);
            } else {
                transaction.setDebitAmount(Amount);
                transaction.setCreditAmount(0.0);
            }
            transaction.setTransactionDate(new Date());
            Currency currency = currencyService.findById(currencyId);
            transaction.setCurrency(currency);
            transaction.setDescription(trnDesc);
            transaction = transactionRepository.save(transaction);
        }
        return transaction;
    }


}
