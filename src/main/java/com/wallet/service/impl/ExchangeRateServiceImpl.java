package com.wallet.service.impl;

import com.wallet.entity.BankAccount;
import com.wallet.entity.Currency;
import com.wallet.entity.ExchangeRate;
import com.wallet.service.*;
import com.wallet.service.repository.ExchangeRateRepository;
import com.wallet.utils.enums.CurrencyTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by taraneh on 7/31/2020.
 */

@Service
public class ExchangeRateServiceImpl implements ExchangeRateService {

    @Autowired
    private CurrencyService currencyService;
    @Autowired
    private ExchangeRateRepository exchangeRateRepository;

    @Value("${currency.rial}")
    private Long rial;

    @Override
    public Double exchangeDollarToRial(CurrencyTypeEnum currency, Double amount){
        Double rate = null;
        if (currency.getValue().equals(CurrencyTypeEnum.DOLLAR.name())) {
            rate= Double.valueOf(amount * this.rial);
        }
        createExchange(rate);
        return  createExchange(rate).getRate();
    }

    @Override
    public Double exchangeRialToDollar(CurrencyTypeEnum currency, Double amount){
        Double rate = null;
        if (currency.getValue().equals(CurrencyTypeEnum.RIAL.name())) {
            rate= Double.valueOf(amount / this.rial);
        }
        createExchange(rate);
        return  createExchange(rate).getRate();
    }

    private ExchangeRate createExchange( Double rate){
        Currency currency1 = currencyService.findById(CurrencyTypeEnum.DOLLAR.getId());
        Currency currency2 = currencyService.findById(CurrencyTypeEnum.RIAL.getId());
        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setFirstCurrency(currency1);
        exchangeRate.setSecondCurrencyId(currency2);
        exchangeRate.setRate(rate);
        exchangeRateRepository.save(exchangeRate);
        return exchangeRate;
    }
}
