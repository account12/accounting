package com.wallet.service.impl;

import com.wallet.entity.BankAccount;
import com.wallet.entity.Document;
import com.wallet.entity.DocumentType;
import com.wallet.model.DebitDto;
import com.wallet.service.DocumentService;
import com.wallet.service.repository.DocumentRepository;
import com.wallet.service.repository.DocumentTypeRepository;
import com.wallet.utils.enums.DocumentTypeEnum;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by taraneh on 7/31/2020.
 */

@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private DocumentTypeRepository documentTypeRepository;

    @Override
    public List<Document> findBydocref(String docRef) {
       return documentRepository.findByDocRef(docRef);
    }

    @Override
    public Document getDocument(DebitDto debitDto, DocumentTypeEnum docType) {
        Document generalDoc = new Document();
        generalDoc.setDocAmount(debitDto.getAmount());

        if (docType != null) {
            DocumentType documentType = documentTypeRepository.getOne(docType.getId());
            generalDoc.setDocumentType(documentType);
        }

        generalDoc.setDocDescription(debitDto.getDebitDesc());
        Date date = new Date();
        generalDoc.setDocDate(date);

        if (debitDto.getReferCode() != null && !debitDto.getReferCode().trim().equalsIgnoreCase("0")) {
            generalDoc.setDocRef(debitDto.getReferCode().trim());
        } else {
            generalDoc.setDocRef(null);
        }
        generalDoc = documentRepository.save(generalDoc);
        return generalDoc;
    }
}
