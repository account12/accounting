package com.wallet.service.impl;

import com.wallet.entity.Currency;
import com.wallet.errors.ErrorConstants;
import com.wallet.errors.exception.CurrencyInsertFailedException;
import com.wallet.errors.exception.CurrencyIsAlreadyException;
import com.wallet.errors.exception.CurrencyNotFoundException;
import com.wallet.model.CurrencyRequest;
import com.wallet.model.CurrencyResponse;
import com.wallet.service.CurrencyService;
import com.wallet.service.repository.CurrencyRepository;
import com.wallet.utils.ConvertUtil;
import com.wallet.utils.enums.CurrencyTypeEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by taraneh on 7/24/2020.
 */

@Service
public class CurrencyServiceImpl implements CurrencyService {

    @Autowired
    private CurrencyRepository currencyRepository;
    @Autowired
    private ConvertUtil convertUtil;

    @Override
    public CurrencyResponse createCurrency(CurrencyRequest currencyRequest) {
        Currency currency = new Currency();
        try {
            currency.setName(currencyRequest.getName());
            findCurrencyName(currencyRepository.save(currency));
        } catch (Exception e) {
            throw new CurrencyInsertFailedException(ErrorConstants.Currency_Insert_Failed_Exception);
        }

        return convertUtil.setCurrencyIsOk();
    }

    private void findCurrencyName(Currency currency) {
        Currency currencyObj = currencyRepository.findByName(currency.getName().name());
        if (currencyObj.getName().equals(currency.getName().name())) {
            throw new CurrencyIsAlreadyException(ErrorConstants.Currency_Is_Already_Exception);
        }
    }

    @Override
    public CurrencyResponse updateCurrency(CurrencyRequest currencyRequest) {
        if (StringUtils.isEmpty(currencyRequest.getName().name())) {
            throw new CurrencyNotFoundException(ErrorConstants.Currency_Not_Found_Exception);
        }
        Optional<Currency> currency = currencyRepository.findById(currencyRequest.getId());
        if (!currency.isPresent()) {
            throw new CurrencyNotFoundException(ErrorConstants.Currency_Not_Found_Exception);
        }
        currency.get().setName(currencyRequest.getName());
        currencyRepository.save(currency.get());
        return convertUtil.setCurrencyIsOk();
    }

    @Override
    public void deleteCurrency(Long id) {
        Optional<Currency> currency = currencyRepository.findById(id);
        currencyRepository.delete(currency.get());
    }

    @Override
    public Currency findById(Long id) {
        return currencyRepository.getOne(id);
    }

    @Override
    public String findByName(CurrencyTypeEnum type) {
        return currencyRepository.findByName(type.name()).getName().name();
    }
}
