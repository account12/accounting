package com.wallet.service;


import com.wallet.model.TransactionDto;
import com.wallet.model.TransferDto;
import com.wallet.utils.enums.TransactionTypeEnum;


public interface WalletService {

	Long credit(TransactionDto dto);
	Long debit(TransactionDto dto);
	Long transfer(TransferDto dto);

}
