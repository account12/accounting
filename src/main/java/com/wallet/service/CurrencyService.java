package com.wallet.service;

import com.wallet.entity.Currency;
import com.wallet.model.CurrencyRequest;
import com.wallet.model.CurrencyResponse;
import com.wallet.utils.enums.CurrencyTypeEnum;

/**
 * Created by taraneh on 7/24/2020.
 */
public interface CurrencyService {

    CurrencyResponse createCurrency(CurrencyRequest currencyRequest);
    CurrencyResponse updateCurrency(CurrencyRequest currencyRequest);
    void deleteCurrency(Long id);
    Currency findById(Long id);
    String findByName(CurrencyTypeEnum type);
}
