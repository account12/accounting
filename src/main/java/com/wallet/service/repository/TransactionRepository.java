package com.wallet.service.repository;

import com.wallet.entity.Transaction;
import com.wallet.utils.enums.TransactionTypeEnum;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by taraneh on 7/24/2020.
 */

@Repository
public interface TransactionRepository extends JpaRepository<Transaction,Long>{

    @Query(value="select (COALESCE(sum(creditAmount), 0) -COALESCE( sum(debitAmount), 0)) as balance  from Transaction where account= ?1  and currency=?2 ")
    Long getBalance(Long accountid,Long currencyid);

    @Query(value="select sum(creditAmount) from Transaction where account= ?1 ")
    Long getSumCredit(Long accountid,Long currencyid);

   /* @Cacheable("findBytrntypeid")
    Transaction findBytrntypeid(TransactionTypeEnum trnType);


    @Cacheable("findBydocid")
    List<Transaction> findBydocid(Long docid);*/
}
