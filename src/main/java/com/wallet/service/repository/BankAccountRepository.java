package com.wallet.service.repository;

import com.wallet.entity.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by taraneh on 7/24/2020.
 */

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount,Long> {
    BankAccount findByAccountCode(String accountCode);

    @Query("select b from BankAccount b, Party p where b.party.id = p.id and p.mobile=:mobile")
    BankAccount findByMobile(@Param("mobile") String mobile);
}
