package com.wallet.service.repository;

import com.wallet.entity.DocumentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by taraneh on 7/24/2020.
 */

@Repository
public interface DocumentTypeRepository extends JpaRepository<DocumentType,Long>{
}
