package com.wallet.service.repository;

import com.wallet.entity.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by taraneh on 7/24/2020.
 */

@Repository
public interface DocumentRepository extends JpaRepository<Document,Long>{
    List<Document> findByDocRef(String docRef);

}
