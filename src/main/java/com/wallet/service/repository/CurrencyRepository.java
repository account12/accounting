package com.wallet.service.repository;

import com.wallet.entity.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by taraneh on 7/24/2020.
 */

@Repository
public interface CurrencyRepository extends JpaRepository<Currency,Long> {
    Currency findByName(String name);
}

