package com.wallet.service;

import com.wallet.entity.BankAccount;
import com.wallet.entity.TransactionType;
import com.wallet.model.BankAccountRequest;

/**
 * Created by taraneh on 7/24/2020.
 */
public interface BankAccountService {

    void createBankAccount(BankAccountRequest bankAccountRequest);
    void updateBankAccount(BankAccountRequest bankAccountRequest);
    BankAccount findByMobile(String msisdn);
    BankAccount finById(Long id);
}
