package com.wallet.service;

import com.wallet.entity.Document;
import com.wallet.model.DebitDto;
import com.wallet.utils.enums.DocumentTypeEnum;

import java.util.List;

/**
 * Created by taraneh on 7/31/2020.
 */
public interface DocumentService {

    List<Document> findBydocref(String docRef);
    Document getDocument(DebitDto debitDto, DocumentTypeEnum docType);
}
